package com.example.buttstallion.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Author: ANna Spillner
 */
public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    private NotificationsHelper nh;
    public AlarmManager am;
    public PendingIntent pi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nh = new NotificationsHelper(this);
        am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }

    /*
    handling buttons
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMain_setAlarm:
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker"); // as soon as time is picked calls startAlarm
                break;
            case R.id.btnMain_cancel:
                TextView tv = (TextView) findViewById(R.id.txtMain_showTime);
                tv.setText("");
                cancelAlarm();
                break;
            case R.id.btnMain_setSnooze:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
    }

    /**
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        TextView setTime = (TextView) findViewById(R.id.txtMain_showTime);
        setTime.setText(String.format("%02d:%02d", hourOfDay, minute)); // e.g. 12:09

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);

        startAlarm(c);
    }

    /**
     * start the alarm
     * @param c (Calendar)
     */
    private void startAlarm(Calendar c) {

        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }
        am.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), PendingIntent.getActivity(this, 0, new Intent(this, SnoozeAcitivty.class), PendingIntent.FLAG_UPDATE_CURRENT));
    }

    /**
     * cancel the snooze alarm
     */
    private void cancelAlarm() {
        am.cancel(PendingIntent.getActivity(this, 0, new Intent(this, SnoozeAcitivty.class), PendingIntent.FLAG_UPDATE_CURRENT));
    }
}
