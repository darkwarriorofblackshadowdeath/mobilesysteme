package com.example.buttstallion.alarm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

/**
 * Author: Anna Spillner
 */
public class NotificationsHelper extends ContextWrapper {
    public static final String channel1ID = "channel1ID";
    public static final String channel1Name = "Channel 1";
    public static final String channel2ID = "channel2ID";
    public static final String channel2Name = "Channel 2";

    private NotificationManager nm;

    /*
     * Constructor
     */
    @SuppressLint("ObsoleteSdkInt")
    public NotificationsHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(channel1ID, channel1Name, NotificationManager.IMPORTANCE_LOW);
            channel1.enableLights(true);
            channel1.enableVibration(true);
            channel1.setLightColor(R.color.colorPrimary);
            channel1.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(channel1);
        }
    }

    /**
     * returns the prepared notification manager
     * @return (NotificationManager)
     */
    public NotificationManager getManager() {
        if (nm == null) {
            nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return nm;
    }

    /**
     * builds the notification
     * @param _title (String)
     * @param _message (String)
     * @return (NotificationCompat.Builder)
     */
    public NotificationCompat.Builder getChannel1Notification(String _title, String _message) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent intent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        return new NotificationCompat.Builder(getApplicationContext(), channel1ID).setContentTitle(_title).setContentText(_message).setSmallIcon(R.drawable.ic_alarm).setContentIntent(intent);
    }
}
