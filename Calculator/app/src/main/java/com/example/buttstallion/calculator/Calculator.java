package com.example.buttstallion.calculator;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class Calculator extends AppCompatActivity {
    TextView calcField,
            calcView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        calcField = (TextView) findViewById(R.id.calcField);
        calcView = (TextView) findViewById(R.id.calcView);
    }

    /**
     * handling buttons
     *
     * @param v
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.one:
                calcField.setText(calcField.getText() + "1");
                break;
            case R.id.two:
                calcField.setText(calcField.getText() + "2");
                break;
            case R.id.three:
                calcField.setText(calcField.getText() + "3");
                break;
            case R.id.four:
                calcField.setText(calcField.getText() + "4");
                break;
            case R.id.five:
                calcField.setText(calcField.getText() + "5");
                break;
            case R.id.six:
                calcField.setText(calcField.getText() + "6");
                break;
            case R.id.seven:
                calcField.setText(calcField.getText() + "7");
                break;
            case R.id.eight:
                calcField.setText(calcField.getText() + "8");
                break;
            case R.id.nine:
                calcField.setText(calcField.getText() + "9");
                break;
            case R.id.zero:
                calcField.setText(calcField.getText() + "0");
                break;
            case R.id.add:
                if (Logic.checkLastChar(calcField, false)) {
                    if (Logic.bracket) {
                        Logic.addBracket(calcField);
                    }
                    calcField.setText(calcField.getText() + "+");
                }
                break;
            case R.id.subtract:
                if (Logic.checkLastChar(calcField, false)) {
                    if (Logic.bracket) {
                        Logic.addBracket(calcField);
                    }
                    calcField.setText(calcField.getText() + "-");
                }
                break;
            case R.id.multiply:
                if (Logic.checkLastChar(calcField, false)) {
                    if (Logic.bracket) {
                        Logic.addBracket(calcField);
                    }
                    calcField.setText(calcField.getText() + "*");
                }
                break;
            case R.id.divide:
                if (Logic.checkLastChar(calcField, false)) {
                    if (Logic.bracket) {
                        Logic.addBracket(calcField);
                    }
                    calcField.setText(calcField.getText() + "/");
                }
                break;
            case R.id.period:
                if (Logic.checkLastChar(calcField, false)) {
                    if (Logic.bracket) {
                        Logic.addBracket(calcField);
                    }
                    calcField.setText(calcField.getText() + ".");
                }
                break;
            case R.id.equals:
                try {
                    if (Logic.checkLastChar(calcField, false)) {
                        if (Logic.bracket) {
                            Logic.addBracket(calcField);
                        }
                        Expression e = new ExpressionBuilder(calcField.getText().toString())
                                .build();
                        double result = e.evaluate();
                        calcView.setText(calcField.getText());
                        calcField.setText(String.valueOf(result));
                    }
                } catch (NumberFormatException e) {

                } finally {
                    calcField.setText("");
                }
                break;
            case R.id.btnsqrt:
                Logic.addFunction(calcField, "sqrt(");
                break;
            case R.id.btnsin:
                Logic.addFunction(calcField, "sin(");
                break;
            case R.id.btncos:
                Logic.addFunction(calcField, "cos(");
                break;
            case R.id.btntan:
                Logic.addFunction(calcField, "tan(");
                break;
            case R.id.clear:
                calcField.setText("");
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Configuration conf = getResources().getConfiguration();
        switch (conf.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return false;
            default:
                getMenuInflater().inflate(R.menu.menu, menu);
                return true;
        }
    }

    /**
     * handling menu
     *
     * @param item (menu)
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sqrt:
                Logic.addFunction(calcField, "sqrt(");
                return true;
            case R.id.sin:
                Logic.addFunction(calcField, "sin(");
                return true;
            case R.id.cos:
                Logic.addFunction(calcField, "cos(");
                return true;
            case R.id.tan:
                Logic.addFunction(calcField, "tan(");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

