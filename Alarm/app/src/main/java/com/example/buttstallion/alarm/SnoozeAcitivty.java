package com.example.buttstallion.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Calendar;

/**
 * Author: Anna Spillner
 */
public class SnoozeAcitivty extends AppCompatActivity {
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snooze);
        mp = MediaPlayer.create(this, R.raw.rickroll);
        mp.setLooping(true);
        mp.start();
    }

    /**
     * handling Buttons
     * @param v (View)
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSnooze_snooze:
                mp.stop();

                int snoozeTime = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("snooze", "5")); // getting the value from preferences
                AlarmManager am=(AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent intent = new Intent(this, SnoozeAcitivty.class);
                am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (snoozeTime * 60 * 1000), PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)); // setting the time for the snooze

                /*
                Notifications
                 */
                Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);

                String time = String.format("%02d:%02d", hour, minute + snoozeTime);

                // jumps an hour forward in case the snooze increase the minutes to over 59
                if (minute + snoozeTime > 59) {
                    time = String.format("%02d:%02d", hour + 1, minute + snoozeTime);
                }

                //notification itself
                NotificationsHelper nh = new NotificationsHelper(this);
                NotificationCompat.Builder nb = nh.getChannel1Notification("Wecker", "Nächstes Klingeln um " + time + " Uhr");
                nh.getManager().notify(1, nb.build());

                finish();
                break;

            case R.id.btnSnooze_off:
                mp.stop();
                finish();
                break;
        }
    }
}
