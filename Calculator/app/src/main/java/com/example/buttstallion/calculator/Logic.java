package com.example.buttstallion.calculator;

import android.widget.TextView;

public class Logic extends Calculator {

    public static boolean bracket = false;

    /**
     * checks if the textview is empty and if last character is an operator or a number.
     *
     * @param _text
     * @param _operator
     * @return
     */
    public static boolean checkLastChar(TextView _text, boolean _operator) {
        if (_text.getText().toString().isEmpty()) {
            return true;
        } else if (_operator) {
            return _text.getText().toString().substring(_text.getText().toString().length() - 1).matches("([+|*|/])") || _text.getText().toString().substring(_text.getText().toString().length() - 1).matches("[-]")||_text.getText().toString().substring(_text.getText().toString().length() - 1).matches("[)]");
        } else {
            return _text.getText().toString().substring(_text.getText().toString().length() - 1).matches("\\d");
        }
    }

    /**
     * adds a bracket if bracket is true and sets bracket false
     * @param _text (TextView)
     */
    public static void addBracket(TextView _text) {
            _text.setText(_text.getText().toString() + ")");
            bracket = false;
    }

    /**
     * calls checkLastChar and adds a function if the last character is an operator
     * @param _text
     * @param _operation
     */
    public static void addFunction(TextView _text, String _operation){
        if (checkLastChar(_text, true)) {
            _text.setText(_text.getText() + _operation);
            bracket = true;
        }
    }
}
